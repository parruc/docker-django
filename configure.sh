#!/bin/bash

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	sudo apt install python3 python3-dev python3-pip python3-virtualenv nginx nginx-extras containerd docker.io
else
	echo "Ensure to install python3 python3-dev python3-pip python3-virtualenv nginx containerd and docker.io equivalent on your OS"
fi
virtualenv --python=python3 .
bin/pip install -r requirements.txt
[ -d  config/domains ] || mkdir -p config/domains
sudo ./init.py
sudo chown -R `id -un` .
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	sudo service nginx reload
else
	echo "Remember to restart NGINX"
fi
ln -sf config/Makefile .
ln -sf config/docker-compose.yml .
